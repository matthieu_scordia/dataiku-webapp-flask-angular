import os
import requests
from datetime import datetime
from flask import Flask, request, Response
from flask import render_template, url_for, redirect, send_from_directory
from flask import send_file, make_response, abort, jsonify
import pandas as pd
from apps import app
import json

# routing for API endpoints (generated from the models designated as API_MODELS)


# routing for basic pages (pass routing onto the Angular app)
@app.route('/')
@app.route('/home')
def basic_pages(**kwargs):
	return make_response(open('apps/templates/index.html').read())

# routing for CRUD-style endpoints
# passes routing onto the angular frontend if the requested resource exists


@app.route('/API/home')
def home():
	t = {"a":1, "b":2, "c":3}
	return jsonify(data = t)




