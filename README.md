# Dataiku Apps

### How to Get Started

1. clone this repo

2. install all the necessary packages (best done inside of a virtual environment)
> pip install -r requirements.txt

3. run the webapp
> python runserver.py

4. check out:
> http://localhost:5000/
